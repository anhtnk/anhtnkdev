"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// bạn có thể dùng để lưu trữ combo được chọn, 
// mỗi khi khách chọn menu S, M, L bạn lại đổi giá trị properties của nó
var gSelectedMenuStructure = {
    menuName: "...",    // S, M, L
    duongKinhCM: 0,
    suongNuong: 0,
    saladGr: 0,
    drink: 0,
    priceVND: 0
}
// bạn có thể dùng để lưu loại pizza đươc chọn, mỗi khi khách chọn, bạn lại đổi giá trị cho nó
var gSelectedPizzaType = "..." ;
var gSizePizza = "";
// biến lưu kết quả kiểm tra voucherCode
var gVoucherInfo = {
    maVoucher : "",
    phanTramGiamGia : 0
};
// biến chứa thông tin combo
var gComboData = {
    kichCo: "",
    duongKinh: 0,
    suonNuong: 0,
    salad: 0,
    nuocNgot: 0,
    thanhTien: 0
}
// biến lưu sô tiền giảm giá
var gTotalPrice = 0;
// biến lưu số tiền sau giảm giá
var gTotal = gComboData.thanhTien - gTotalPrice;
// biến chứa loại nươc uống
var gDrink = "";
// Khởi tạo đối tượng chứa thông tin form
var gUserObject = {
    hoTen : "",
    email : "",
    soDienThoai : "",
    diaChi : "",
    idVourcher : "",
    loiNhan : "",
}
// URL_DRINK
const gURL_DRINK = "http://42.115.221.44:8080/devcamp-pizza365/drinks";
// voucher 
const gBASE_URL_VOUCHER = "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/";
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
onLoading();
// các nút chọn size
$("#btnSizeSmall").on("click", onBtnSizeSmallClick);
$("#btnSizeMedium").on("click", onBtnSizeMediumClick);
$("#btnSizeLarge").on("click", onBtnSizeLargeClick);
// các nút chọn type
$("#btnTypeOne").on("click", onBtnTypeOneClick);
$("#btnTypeTwo").on("click", onBtnTypeTwoClick);
$("#btnTypeThree").on("click", onBtnTypeThreeClick);
// gán sự kiên change cho ô select
$("#select-drink").on("change", function(){
    gDrink = $("#select-drink option:selected").text();
});
// sự kiện cho nút gửi
$("#btnSendClick").on("click", onSendClick);
// sự kiện cho nút Tạo đơn
$("#btn-confirm").on("click", onConfirmClick);
// reset modal 
$('#modal-detail-order').on('hidden.bs.modal', function (event) {
    resetOrderModal();
});
// 
$('#create-order-modal').on('hidden.bs.modal', function (event) {
    location.reload();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onLoading(){
    // gọi api và load được dữ liệu drink vào select
    callApiDrinkFromServer();
    // 
    
}
// các hàm xử lí sự kiện khi click size pizza
function onBtnSizeSmallClick(){
    "use strict";
    gSizePizza = "S";
    gSelectedMenuStructure.menuName = "S"; 
    // b1: thu thập dữ liệu
    getComboData(gComboData, gSelectedMenuStructure.menuName);
    // b2: validate dữ liệu
    // b3: hiển thị dữ liệu
    // hàm thay đổi màu nút khi chọn Small
    ChangeColorBtnClick(gSelectedMenuStructure.menuName);
    
}
function onBtnSizeMediumClick(){
    "use strict";
    gSizePizza = "M";
    gSelectedMenuStructure.menuName = "M"; 
    // b1: thu thập dữ liệu
    getComboData(gComboData, gSelectedMenuStructure.menuName);
    // b2: validate dữ liệu
    // b3: hiển thị dữ liệu
    // hàm thay đổi màu nút khi chọn Small
    ChangeColorBtnClick(gSelectedMenuStructure.menuName);
    
}
function onBtnSizeLargeClick(){
    "use strict";
    gSizePizza = "L";
    gSelectedMenuStructure.menuName = "L"; 
    // b1: thu thập dữ liệu
    getComboData(gComboData, gSelectedMenuStructure.menuName);
    // b2: validate dữ liệu
    // b3: hiển thị dữ liệu
    // hàm thay đổi màu nút khi chọn Small
    ChangeColorBtnClick(gSelectedMenuStructure.menuName);
    
}

// các hàm xử lí sự kiện khi click type pizza 
function onBtnTypeOneClick(){
    gSelectedPizzaType = "Seafood";
    changeColorTypeBtnClick(gSelectedPizzaType);
}
function onBtnTypeTwoClick(){
    gSelectedPizzaType = "Hawiian";
    changeColorTypeBtnClick(gSelectedPizzaType);
}
function onBtnTypeThreeClick(){
    gSelectedPizzaType = "Bacon";
    changeColorTypeBtnClick(gSelectedPizzaType);
}

// hàm xử lí sự kiện khi click gửi
function onSendClick(){
    console.log("Nút send được click");
    // B1 : thu thập dữ liệu trên form
    getDataUserOnForm(gUserObject)
    // B2 : validate
    var vIsValid = validateUserOnForm(gUserObject);
    if(vIsValid) {
        // B3 : hiển thị thông tin form ra modal
        showModalInfoUser(gUserObject);
        $("#modal-detail-order").modal("show");
    }
    
}

// hàm xử lý sự kiện click cho nút tạo đơn
function onConfirmClick(){
    var vObjectOrder = {
      kichCo: "",
      duongKinh: 0,
      suon: 0,
      salad: 0,
      loaiPizza: "",
      idVourcher: "",
      idLoaiNuocUong: "",
      soLuongNuoc: 0,
      hoTen: "",
      thanhTien: 0,
      email: "",
      soDienThoai: "",
      diaChi: "",
      loiNhan: ""
    }
  
    // b1: thu thập dữ liệu order gữi server call Api
    getOrderData(vObjectOrder);
    // b2: validate dữ liệu
    // b3: call Api create order và xử lý
    callApiCreateOrder(vObjectOrder);
    $("#modal-detail-order").modal("hide");
    $("#create-order-modal").modal("show");
  
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
/// hàm thay đổi màu nút size được click
function ChangeColorBtnClick(paramBtnClick){
    "use strict";
    // truy vấn
    console.log("Size được chọn : "+ gSizePizza);
    var vBtnSmallElement = $("#btnSizeSmall");
    var vBtnMediumElement = $("#btnSizeMedium");
    var vBtnLargeElement = $("#btnSizeLarge");
  
    // className mỗi nút sẽ thay đổi tùy theo người dùng Click 
    if(paramBtnClick == "S"){
        vBtnSmallElement.removeClass("btn btn-warning").addClass("btn btn-success"); 
        vBtnMediumElement.removeClass("btn btn-success").addClass("btn btn-warning"); 
        vBtnLargeElement.removeClass("btn btn-success").addClass("btn btn-warning");  
    }else if(paramBtnClick == "M"){      
        vBtnSmallElement.removeClass("btn btn-success").addClass("btn btn-warning"); 
        vBtnMediumElement.removeClass("btn btn-warning").addClass("btn btn-success"); 
        vBtnLargeElement.removeClass("btn btn-success").addClass("btn btn-warning"); 
      
    }else if(paramBtnClick == "L"){
        vBtnSmallElement.removeClass("btn btn-success").addClass("btn btn-warning"); 
        vBtnMediumElement.removeClass("btn btn-success").addClass("btn btn-warning"); 
        vBtnLargeElement.removeClass("btn btn-warning").addClass("btn btn-success"); 
    }
}
/// hàm thay đổi màu nút type được click
function changeColorTypeBtnClick(paramBtnClick){
    "use strict";
    // truy vấn
    console.log("Type được chọn : "+ gSelectedPizzaType);
    var vBtnTypeOneElement = $("#btnTypeOne");
    var vBtnTypeTwoElement = $("#btnTypeTwo");
    var vBtnTypeThreeElement = $("#btnTypeThree");
  
    // className mỗi nút sẽ thay đổi tùy theo người dùng Click 
    if(paramBtnClick == "Seafood"){
        vBtnTypeOneElement.removeClass("btn btn-warning").addClass("btn btn-success"); 
        vBtnTypeTwoElement.removeClass("btn btn-success").addClass("btn btn-warning"); 
        vBtnTypeThreeElement.removeClass("btn btn-success").addClass("btn btn-warning");  
    }else if(paramBtnClick == "Hawiian"){      
        vBtnTypeOneElement.removeClass("btn btn-success").addClass("btn btn-warning"); 
        vBtnTypeTwoElement.removeClass("btn btn-warning").addClass("btn btn-success"); 
        vBtnTypeThreeElement.removeClass("btn btn-success").addClass("btn btn-warning"); 
      
    }else if(paramBtnClick == "Bacon"){
        vBtnTypeOneElement.removeClass("btn btn-success").addClass("btn btn-warning"); 
        vBtnTypeTwoElement.removeClass("btn btn-success").addClass("btn btn-warning"); 
        vBtnTypeThreeElement.removeClass("btn btn-warning").addClass("btn btn-success"); 
    }
}

// hàm xử lý thu thập dữ liệu combo
function getComboData(paramComboData, paramIdBtnChon){
  switch (paramIdBtnChon){
    // thu thập dữ liệu combo S
    case "S":
      paramComboData.kichCo = "S";
      paramComboData.duongKinh = 20;
      paramComboData.suonNuong = 2;
      paramComboData.salad = 200;
      paramComboData.nuocNgot = 2;
      paramComboData.thanhTien = 150000;
      break;
    // thu thập dữ liệu combo M
    case "M":
      paramComboData.kichCo = "M";
      paramComboData.duongKinh = 25;
      paramComboData.suonNuong = 4;
      paramComboData.salad = 300;
      paramComboData.nuocNgot = 3;
      paramComboData.thanhTien = 200000;      
      break;
    // thu thập dữ liệu combo L
    case "L":
      paramComboData.kichCo = "L";
      paramComboData.duongKinh = 30;
      paramComboData.suonNuong = 8;
      paramComboData.salad = 400;
      paramComboData.nuocNgot = 4;
      paramComboData.thanhTien = 250000;      
      break;
  }
  
}


// hàm load dữ liệu nước uống vào select drink
function loadDataDrinkToSelect(paramDrinkToSelect){
    var vDrinkSelect = $("#select-drink");
    vDrinkSelect.append($("<option>", {text: "Tất cả các loại nước uống", value: ""}));
    for(var bI = 0; bI < paramDrinkToSelect.length; bI ++){
        var bOptionDrink = $("<option>", 
            {
                text: paramDrinkToSelect[bI].tenNuocUong, 
                value: paramDrinkToSelect[bI].maNuocUong
            }
        );
        bOptionDrink.appendTo(vDrinkSelect);
    }

}

// hàm lấy dữ liệu Api drink từ server
function callApiDrinkFromServer(){
    $.ajax({
        url: gURL_DRINK,
        type: "GET",
        dataType: 'json',
        success: function(responseObject){
            // load dữ liệu drink vào select
            loadDataDrinkToSelect(responseObject);
        },
        error: function(error){
          console.assert(error.responseText);
        }
    });
}

// hàm thu thập dữ liệu người dùng trên form
function getDataUserOnForm(paramUserObject){
    paramUserObject.hoTen = $("#inp-ten").val().trim();
    paramUserObject.email = $("#inp-email").val().trim();
    paramUserObject.soDienThoai = $("#inp-soDienThoai").val().trim();
    paramUserObject.diaChi = $("#inp-diaChi").val().trim();
    paramUserObject.loiNhan = $("#inp-loiNhan").val().trim();
    paramUserObject.idVourcher = $("#inp-maGiamGia").val().trim();
    getVoucherByVoucherId(paramUserObject.idVourcher);
}

// hàm validate dữ liệu
function validateUserOnForm(paramUserObject){
    if(paramUserObject.hoTen === ""){
        alert("Please enter fullname");
        return false;
    }
    if(paramUserObject.email === ""){
        alert("Please enter email");
        return false;
    }
    if (getValidateEmail(paramUserObject.email) == false) {
        alert("Định dạng Email không đúng!!")
        return false;
    }
    
    if(paramUserObject.soDienThoai === ""){
        alert("Định dạng số điện thoại không đúng");
        return false;
    }    
    if(paramUserObject.diaChi === ""){
        alert("Please enter address");
        return false;
    }
    if (gComboData.kichCo === ""){
        alert("Bạn chưa chọn Combo size!!");
        return false;
    }
    if (gSelectedPizzaType === "..."){
        alert("Bạn chưa chọn loại pizza!!");
        return false;
    }
    if (gDrink == 0){
        alert("Bạn chưa chọn nước uống!!");
        return false;
    }
    return true;
}

// validate Email
function getValidateEmail(paramEmail) {
    const gEMAIL = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return gEMAIL.test(paramEmail);
}

function getVoucherByVoucherId(paramVoucherId){
  if (paramVoucherId == ""){
    gVoucherInfo.maVoucher = "Không có mã voucher!!"
  }
  else
  {
    gVoucherInfo.maVoucher =  callApiVoucherFromServer(paramVoucherId);
  }
  return gVoucherInfo.maVoucher;
}



// lấy mã voucher và phần trăm tương ứng từ server 
function callApiVoucherFromServer(paramVoucherId){
    $.ajax({
        url: gBASE_URL_VOUCHER + paramVoucherId,
        type: "GET",
        dataType: 'json',
        async: false,
        success: function(responseObject){
            gVoucherInfo.maVoucher =  responseObject.maVoucher;
            gVoucherInfo.phanTramGiamGia = responseObject.phanTramGiamGia;
            //console.log(responseObject);
            getTotalPrice(responseObject);
        },
        error: function(data){
            gVoucherInfo.maVoucher =  " Mã voucher không đúng!!";
        }
    });
    return gVoucherInfo.maVoucher;
}
// hàm tính tiền sau khi giảm giá
function getTotalPrice(paramResponseObject){
  gTotalPrice = gComboData.thanhTien * (paramResponseObject.phanTramGiamGia / 100);
}


function showModalInfoUser(paramFormCustomerData){
    
    $("#inp-fullname").val(paramFormCustomerData.hoTen);
    $("#inp-emailModal").val(paramFormCustomerData.email);
    $("#inp-phonenumber").val(paramFormCustomerData.soDienThoai);
    $("#inp-address").val(paramFormCustomerData.diaChi);
    $("#inp-message").val(paramFormCustomerData.loiNhan);
    $("#inp-voucher").val(paramFormCustomerData.idVourcher);
    $("#showInfoUser").val("Xác nhận: " + paramFormCustomerData.hoTen + ", " + 
                                                     paramFormCustomerData.soDienThoai + ", " + 
                                                     paramFormCustomerData.diaChi + "\n" +
                                        "Menu: " + gComboData.kichCo + ", " + 
                                        "sườn nướng: " + gComboData.suonNuong + ", " + 
                                        "salad: " + gComboData.salad + ", " + 
                                        "nước ngọt: " + gComboData.nuocNgot + "\n" + 
                                        "Loại pizza: " + gSelectedPizzaType  + ", " +
                                        "loại nước: " + gDrink + "\n" + 
                                        "Giá: " + gComboData.thanhTien + ", " + 
                                        "Mã giảm giá: " + gVoucherInfo.maVoucher + "\n" + 
                                        "Phải thanh toán: " + gTotal
                                        );
}

// call Api tạo mới order
function callApiCreateOrder(paramObjectOrder){
  $.ajax({
    url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
    method: "POST",
    headers: {"Content-Type": "application/json;charset=UTF-8"},
    data: JSON.stringify(paramObjectOrder),
    success: function(responseObject){
      loadOrderIdToModal(responseObject);
    }
  });
}

// hàm load orderId vào modal
function loadOrderIdToModal(paramResponseObject){
  $("#inp-order-id").val(paramResponseObject.orderId);
}



// hàm thu thập dữ liệu để tạo order
function getOrderData(paramObjectOrder){
  paramObjectOrder.kichCo = gComboData.kichCo;
  paramObjectOrder.duongKinh = gComboData.duongKinh;
  paramObjectOrder.suon = gComboData.suonNuong;
  paramObjectOrder.salad = gComboData.salad;
  paramObjectOrder.loaiPizza = gSelectedPizzaType;
  paramObjectOrder.idVourcher = gVoucherInfo.maVoucher;
  paramObjectOrder.idLoaiNuocUong = gDrink;
  paramObjectOrder.soLuongNuoc = gComboData.soLuongNuoc;
  paramObjectOrder.hoTen = gUserObject.hoTen;
  paramObjectOrder.thanhTien = gComboData.thanhTien;
  paramObjectOrder.email = gUserObject.email;
  paramObjectOrder.soDienThoai = gUserObject.soDienThoai;
  paramObjectOrder.diaChi = gUserObject.diaChi;
  paramObjectOrder.loiNhan = gUserObject.loiNhan;

}

// reset modal
function resetOrderModal(){
  $("#inp-fullname").val("");
  $("#inp-phonenumber").val("");
  $("#inp-address").val("");
  $("#inp-message").val("");
  $("#inp-voucher").val("");
  $("#showInfoUser").val("");

  gTotal = gComboData.thanhTien;
  gVoucherInfo.phanTramGiamGia = 0;
}







